package com.syeng.socialapp;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.mobileconnectors.dynamodbv2.dynamodbmapper.DynamoDBAttribute;
import com.amazonaws.mobileconnectors.dynamodbv2.dynamodbmapper.DynamoDBHashKey;
import com.amazonaws.mobileconnectors.dynamodbv2.dynamodbmapper.DynamoDBMapper;
import com.amazonaws.mobileconnectors.dynamodbv2.dynamodbmapper.DynamoDBMapperConfig;
import com.amazonaws.mobileconnectors.dynamodbv2.dynamodbmapper.DynamoDBQueryExpression;
import com.amazonaws.mobileconnectors.dynamodbv2.dynamodbmapper.DynamoDBSaveExpression;
import com.amazonaws.mobileconnectors.dynamodbv2.dynamodbmapper.DynamoDBScanExpression;
import com.amazonaws.mobileconnectors.dynamodbv2.dynamodbmapper.DynamoDBTable;
import com.amazonaws.mobileconnectors.dynamodbv2.dynamodbmapper.PaginatedList;
import com.amazonaws.mobileconnectors.dynamodbv2.dynamodbmapper.PaginatedScanList;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.ConditionalCheckFailedException;
import com.amazonaws.services.dynamodbv2.model.ExpectedAttributeValue;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DynamoDBManager {

    private static final String TAG = "DynamoDBManager";

    //TODO: Change to Query instead of Scan
    public static ArrayList<Users> getLocationActiveUsersByRadius(AmazonClientManager amazonClientManager, double longitude, double latitude) {
        try {
            AmazonDynamoDBClient ddb = amazonClientManager.ddb();
            DynamoDBMapper mapper = DynamoDBMapper.builder().dynamoDBClient(ddb).build();

            int locationRadius = Constants.LOCATION_RADIUS;

            Map<String, AttributeValue> expression = new HashMap<>();
            expression.put(":isLocationActive", new AttributeValue().withN("1"));
            /*expression.put(":longitude", new AttributeValue().withN(Double.toString(longitude)));
            expression.put(":longitudeNegativeMax", new AttributeValue().withN(Double.toString(longitude - locationRadius)));
            expression.put(":longitudePositiveMax", new AttributeValue().withN(Double.toString(longitude + locationRadius)));
            expression.put(":latitude", new AttributeValue().withN(Double.toString(latitude)));
            expression.put(":latitudeNegativeMax", new AttributeValue().withN(Double.toString(latitude - locationRadius)));
            expression.put(":latitudePositiveMax", new AttributeValue().withN(Double.toString(latitude + locationRadius)));*/

            String iLAExp = "IsLocationActive = :isLocationActive";
/*            String longNegativeExp = "Longitude between :longitudePositiveMax and :longitude and ";
            String longPositiveExp = "Longitude between :longitudeNegativeMax and :longitude and ";
            String sameLongExp = "Longitude = :longitude and ";
            String sameLatExp = "Latitude = :latitude";
            String latNegativeExp = "Latitude between :latitudePositiveMax and :latitude and ";
            String latPositiveExp = "Latitude between :latitudeNegativeMax and :latitude";*/

            DynamoDBScanExpression scanExpression = new DynamoDBScanExpression()
                    .withFilterExpression(iLAExp)
                    .withExpressionAttributeValues(expression);

            PaginatedScanList<Users> result = mapper.scan(Users.class, scanExpression);

            ArrayList<Users> resultList = new ArrayList<>();
            //resultList.addAll(result);
            for (Users up : result) {
                resultList.add(up);
            }

            return resultList;

            //TODO: use ConsistentRead!

        } catch (AmazonServiceException ex) {
            amazonClientManager.wipeCredentialsOnAuthError(ex);
            return new ArrayList<>();
        }
    }

    public static ArrayList<Events> getActiveEvents(AmazonClientManager amazonClientManager) {
        AmazonDynamoDBClient ddb = amazonClientManager.ddb();
        DynamoDBMapper mapper = DynamoDBMapper.builder().dynamoDBClient(ddb).build();

        try {
            Map<String, AttributeValue> eav = new HashMap<>();
            eav.put(":IsActive", new AttributeValue().withN("1"));

            DynamoDBScanExpression scanExpression = new DynamoDBScanExpression()
                    .withFilterExpression("IsActive = :IsActive")
                    .withExpressionAttributeValues(eav);

            PaginatedList<Events> result = mapper.scan(Events.class, scanExpression);
            ArrayList<Events> resultList = new ArrayList<>();
            resultList.addAll(result);

            return resultList;
        } catch (AmazonServiceException ex) {
            amazonClientManager.wipeCredentialsOnAuthError(ex);
            return new ArrayList<>();
        }
    }

    public static boolean saveUser(Users user, AmazonClientManager amazonClientManager) {
        AmazonDynamoDBClient ddb = amazonClientManager.ddb();
        DynamoDBMapper mapper = DynamoDBMapper.builder()
                .dynamoDBClient(ddb)
                .dynamoDBMapperConfig(new DynamoDBMapperConfig(DynamoDBMapperConfig.SaveBehavior.UPDATE_SKIP_NULL_ATTRIBUTES))
                .build();

        try {
            DynamoDBSaveExpression saveExpression = new DynamoDBSaveExpression();
            saveExpression.withExpectedEntry("UserID", new ExpectedAttributeValue().withValue(new AttributeValue(user.getUserID())));
            mapper.save(user, saveExpression);
            return true;
        } catch (ConditionalCheckFailedException conditionalFailed) {
            //Condition in try block checking for existing user. If it fails or the user doesn't exist, add CreateDate value.
            //TODO: Change from catching to properly checking the table for the user
            user.setCreatedDate(Constants.GetCurrentDateTime());
            mapper.save(user);
            return true;
        } catch (AmazonServiceException ex) {
            amazonClientManager.wipeCredentialsOnAuthError(ex);
            return false;
        }
    }

    public static boolean saveEvent(Events event, AmazonClientManager amazonClientManager) {
        AmazonDynamoDBClient ddb = amazonClientManager.ddb();
        DynamoDBMapper mapper = DynamoDBMapper.builder()
                .dynamoDBClient(ddb)
                .dynamoDBMapperConfig(new DynamoDBMapperConfig(DynamoDBMapperConfig.SaveBehavior.UPDATE_SKIP_NULL_ATTRIBUTES))
                .build();

        try {
            mapper.save(event);
            return true;
        } catch (AmazonServiceException ex) {
            amazonClientManager.wipeCredentialsOnAuthError(ex);
            return false;
        }
    }

    @DynamoDBTable(tableName = Constants.TBL_USERS)
    public static class Users {
        private String userID;
        private String firstName;
        private String lastName;
        private String email;
        private int isLocationActive;
        private double longitude;
        private double latitude;
        private String createdDate;
        private String modifiedDate;

        @DynamoDBHashKey(attributeName = "UserID")
        public String getUserID() {
            return userID;
        }

        public void setUserID(String userID) {
            this.userID = userID;
        }

        @DynamoDBAttribute(attributeName = "FirstName")
        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        @DynamoDBAttribute(attributeName = "LastName")
        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        @DynamoDBAttribute(attributeName = "Email")
        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        @DynamoDBAttribute(attributeName = "IsLocationActive")
        public int getLocationActive() {
            return isLocationActive;
        }

        public void setLocationActive(int locationActive) {
            isLocationActive = locationActive;
        }

        @DynamoDBAttribute(attributeName = "Longitude")
        public double getLongitude() {
            return longitude;
        }

        public void setLongitude(double longitude) {
            this.longitude = longitude;
        }

        @DynamoDBAttribute(attributeName = "Latitude")
        public double getLatitude() {
            return latitude;
        }

        public void setLatitude(double latitude) {
            this.latitude = latitude;
        }

        @DynamoDBAttribute(attributeName = "CreatedDate")
        public String getCreatedDate() {
            return createdDate;
        }

        public void setCreatedDate(String createdDate) {
            this.createdDate = createdDate;
        }

        @DynamoDBAttribute(attributeName = "ModifiedDate")
        public String getModifiedDate() {
            return modifiedDate;
        }

        public void setModifiedDate(String modifiedDate) {
            this.modifiedDate = modifiedDate;
        }
    }

    @DynamoDBTable(tableName = Constants.TBL_EVENTS)
    public static class Events {
        private String userID;
        private String creatorID;
        private double longitude;
        private double latitude;
        private String locationName;
        private String address;
        private int attendanceLimit;
        private String date;
        private String time;
        private String title;
        private String createdDate;
        private String modifiedDate;
        private int isApproved;
        private int isActive;

        @DynamoDBHashKey(attributeName = "UserID")
        public String getUserID() {
            return userID;
        }

        public void setUserID(String userID) {
            this.userID = userID;
        }

        @DynamoDBAttribute(attributeName = "CreatorID")
        public String getCreatorID() {
            return creatorID;
        }

        public void setCreatorID(String creatorID) {
            this.creatorID = creatorID;
        }

        @DynamoDBAttribute(attributeName = "Longitude")
        public double getLongitude() {
            return longitude;
        }

        public void setLongitude(double logitude) {
            this.longitude = logitude;
        }

        @DynamoDBAttribute(attributeName = "Latitude")
        public double getLatitude() {
            return latitude;
        }

        public void setLatitude(double latitude) {
            this.latitude = latitude;
        }

        @DynamoDBAttribute(attributeName = "AttendanceLimit")
        public int getAttendanceLimit() {
            return attendanceLimit;
        }

        public void setAttendanceLimit(int attendanceLimit) {
            this.attendanceLimit = attendanceLimit;
        }

        @DynamoDBAttribute(attributeName = "LocationName")
        public String getLocationName() {
            return locationName;
        }

        public void setLocationName(String locationName) {
            this.locationName = locationName;
        }

        @DynamoDBAttribute(attributeName = "Address")
        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        @DynamoDBAttribute(attributeName = "Date")
        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        @DynamoDBAttribute(attributeName = "Time")
        public String getTime() {
            return time;
        }

        public void setTime(String time) {
            this.time = time;
        }

        @DynamoDBAttribute(attributeName = "Title")
        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        @DynamoDBAttribute(attributeName = "CreatedDate")
        public String getCreatedDate() {
            return createdDate;
        }

        public void setCreatedDate(String createdDate) {
            this.createdDate = createdDate;
        }

        @DynamoDBHashKey(attributeName = "ModifiedDate")
        public String getModifiedDate() {
            return modifiedDate;
        }

        public void setModifiedDate(String modifiedDate) {
            this.modifiedDate = modifiedDate;
        }

        @DynamoDBAttribute(attributeName = "IsApproved")
        public int getIsApproved() {
            return isApproved;
        }

        public void setIsApproved(int isApproved) {
            this.isApproved = isApproved;
        }

        @DynamoDBAttribute(attributeName = "IsActive")
        public int getIsActive() {
            return isActive;
        }

        public void setIsActive(int isActive) {
            this.isActive = isActive;
        }
    }
}
