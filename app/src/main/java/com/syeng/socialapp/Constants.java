package com.syeng.socialapp;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

import com.amazonaws.regions.Regions;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.TimeZone;

public class Constants {

    public static final String IDENTITY_POOL_ID = "us-west-2:ab0852b4-bc07-4878-aaf0-9436078a038d";
    public static final String TBL_USERS = "tbl_Users";
    public static final String TBL_EVENTS = "tbl_Events";
    public static final String EVENT_INFO_TAG_FRAGMENT = "event_info_tag";
    public static final String GOOGLE_MAPS_TAG_FRAGMENT = "google_maps_tag";
    public static final Regions DYNAMODB_REGION = Regions.US_WEST_2;
    public static final Regions COGNITO_REGION = Regions.US_WEST_2;
    public static final int LOCATION_RADIUS = 10;
    public static final int MY_ACCESS_FINE_LOCATION = 0;

    //Request codes for Intents
    public static final int CREATE_EVENT_REQUEST = 0;
    public static final int PLACE_AUTOCOMPLETE_REQUEST = 1;

    public static String GetCurrentDateTime() {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
        dateFormat.setTimeZone(TimeZone.getTimeZone("America/Los_Angeles"));
        return dateFormat.format(calendar.getTime());
    }

    public static void IsLocationPermissionGranted(Context context, Activity activity) {
        if (ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(activity,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    MY_ACCESS_FINE_LOCATION);
        }
        // TODO: Consider calling
        //    ActivityCompat#requestPermissions
        // here to request the missing permissions, and then overriding
        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
        //                                          int[] grantResults)
        // to handle the case where the user grants the permission. See the documentation
        // for ActivityCompat#requestPermissions for more details.
    }
}
