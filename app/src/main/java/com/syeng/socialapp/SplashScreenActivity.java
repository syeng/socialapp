package com.syeng.socialapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.facebook.AccessToken;

public class SplashScreenActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Class switchClass = (AccessToken.getCurrentAccessToken() == null) ? FacebookLoginActivity.class : MainActivity.class;

        Intent intent = new Intent(this, switchClass);
        startActivity(intent);
        finish();
    }
}
