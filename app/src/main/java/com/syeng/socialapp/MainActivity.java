package com.syeng.socialapp;

import android.annotation.SuppressLint;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.facebook.AccessToken;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;
import com.syeng.socialapp.fragments.GoogleMapsFragment;
import com.syeng.socialapp.fragments.SettingsFragment;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private MainActivity mainActivity;
    private int currentNavigationSelected = 0;
    private Handler handler;
    private Runnable runnable;
    private AmazonClientManager clientManager = null;

    //Used to send into AsyncTask
    class TaskBundle {
        private AmazonClientManager acmBundle;
        private DynamoDBManager.Users userBundle;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Constants.IsLocationPermissionGranted(getApplicationContext(), this);

        mainActivity = this;
        clientManager = new AmazonClientManager(this);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        replaceTransaction(GoogleMapsFragment.newInstance(), Constants.GOOGLE_MAPS_TAG_FRAGMENT);
        navigationView.getMenu().getItem(0).setChecked(true);
        currentNavigationSelected = R.id.nav_map;

        handler = new Handler();
        runnable = new Runnable() {
            @Override
            public void run() {
                saveUserLocation();

                handler.postDelayed(runnable, 50000);
            }
        };
        runnable.run();
    }

    @Override
    protected void onResume() {
        saveUserLocation();
        handler.postDelayed(runnable, 50000);

        super.onResume();
    }

    @Override
    protected void onPause() {
        handler.removeCallbacks(runnable);

        DynamoDBManager.Users user = new DynamoDBManager.Users();
        user.setUserID(AccessToken.getCurrentAccessToken().getUserId());
        user.setLocationActive(0);
        user.setModifiedDate(Constants.GetCurrentDateTime());

        TaskBundle taskBundle = new TaskBundle();
        taskBundle.userBundle = user;
        taskBundle.acmBundle = clientManager;
        new SaveUserTask().execute(taskBundle);

        super.onPause();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_map && id != currentNavigationSelected) {
            replaceTransaction(GoogleMapsFragment.newInstance(), Constants.GOOGLE_MAPS_TAG_FRAGMENT);
        } else if (id == R.id.nav_gallery && id != currentNavigationSelected) {

        } else if (id == R.id.nav_slideshow && id != currentNavigationSelected) {

        } else if (id == R.id.nav_settings && id != currentNavigationSelected) {
            replaceTransaction(SettingsFragment.newInstance(), Constants.GOOGLE_MAPS_TAG_FRAGMENT);
        } else if (id == R.id.nav_share && id != currentNavigationSelected) {

        } else if (id == R.id.nav_send && id != currentNavigationSelected) {

        }

        currentNavigationSelected = id;

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case Constants.MY_ACCESS_FINE_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request.
        }
    }

    private static class SaveUserTask extends AsyncTask<TaskBundle, Void, Void> {
        @Override
        protected Void doInBackground(TaskBundle... taskBundles) {
            DynamoDBManager.saveUser(taskBundles[0].userBundle, taskBundles[0].acmBundle);
            return null;
        }
    }

    private void replaceTransaction(Fragment selectedFragment, String fragmentTag) {
        String backStackName = selectedFragment.getClass().getName();
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.frame_container, selectedFragment, fragmentTag)
                .addToBackStack(backStackName)
                .commit();
    }

    @SuppressLint("MissingPermission")
    private void saveUserLocation() {
        LocationServices.getFusedLocationProviderClient(getApplicationContext()).getLastLocation().addOnSuccessListener(mainActivity, new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                if (location != null) {
                    DynamoDBManager.Users user = new DynamoDBManager.Users();
                    user.setUserID(AccessToken.getCurrentAccessToken().getUserId());
                    user.setLocationActive(1);
                    user.setLongitude(location.getLongitude());
                    user.setLatitude(location.getLatitude());
                    user.setModifiedDate(Constants.GetCurrentDateTime());

                    TaskBundle taskBundle = new TaskBundle();
                    taskBundle.userBundle = user;
                    taskBundle.acmBundle = clientManager;

                    new SaveUserTask().execute(taskBundle);
                }
            }
        });
    }
}
