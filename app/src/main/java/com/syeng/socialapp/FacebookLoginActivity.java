package com.syeng.socialapp;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

public class FacebookLoginActivity extends AppCompatActivity {

    private CallbackManager callbackManager;
    public AmazonClientManager clientManager = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_facebook_login);

        callbackManager = CallbackManager.Factory.create();
        clientManager = new AmazonClientManager(this);

        LoginButton loginButton = findViewById(R.id.fb_login_button);
        loginButton.setReadPermissions("email");

        // Callback registration
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                if(loginResult.getAccessToken() != null) {
                    new SaveUserTask().execute(clientManager);

                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    startActivity(intent);
                    finish();
                }
            }

            @Override
            public void onCancel() {
                // App code
            }

            @Override
            public void onError(FacebookException exception) {
                // App code
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }

    private static class SaveUserTask extends AsyncTask<AmazonClientManager, Void, Void> {
        @Override
        protected Void doInBackground(AmazonClientManager... amazonClientManagers) {
            DynamoDBManager.Users user = new DynamoDBManager.Users();
            user.setLocationActive(1);
            user.setUserID(AccessToken.getCurrentAccessToken().getUserId());
            user.setModifiedDate(Constants.GetCurrentDateTime());

            DynamoDBManager.saveUser(user, amazonClientManagers[0]);
            return null;
        }
    }
}
