package com.syeng.socialapp;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;

import com.facebook.AccessToken;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.model.LatLng;

import java.lang.ref.WeakReference;

public class CreateEventActivity extends AppCompatActivity {

    private EditText eventLocation;
    private EditText eventTitle;
    private EditText eventDate;
    private EditText eventTime;
    private EditText eventAttendanceLimit;
    private TextView eventCancel;
    private TextView eventCreate;
    private DynamoDBManager.Events eventData;
    private AmazonClientManager clientManager = null;

    //Used to send into AsyncTask
    class TaskBundle {
        private AmazonClientManager acmBundle;
        private DynamoDBManager.Events eventBundle;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_event);

        eventData = new DynamoDBManager.Events();
        clientManager = new AmazonClientManager(this);

        eventTitle = findViewById(R.id.event_title);
        eventAttendanceLimit = findViewById(R.id.event_attendance_limit);

        setEventLocationListener();
        setEventDateListener();
        setEventTimeListener();
        setEventCancelListener();
        setEventCreateListener();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == Constants.PLACE_AUTOCOMPLETE_REQUEST) {
            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(CreateEventActivity.this, data);

                String locationName = place.getName().toString();
                eventData.setLocationName(locationName);
                eventLocation.setText(locationName);

                LatLng latlng = place.getLatLng();
                eventData.setLongitude(latlng.longitude);
                eventData.setLatitude(latlng.latitude);

                eventData.setAddress(place.getAddress().toString());
            }
        }
    }

    private void setEventLocationListener() {
        eventLocation = findViewById(R.id.event_location);
        eventLocation.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    try {
                        Intent intent =
                                new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                                        .build(CreateEventActivity.this);
                        startActivityForResult(intent, Constants.PLACE_AUTOCOMPLETE_REQUEST);
                    } catch (GooglePlayServicesRepairableException e) {
                        // TODO: Handle the error.
                    } catch (GooglePlayServicesNotAvailableException e) {
                        // TODO: Handle the error.
                    }
                }
            }
        });
    }

    private void setEventDateListener() {
        eventDate = findViewById(R.id.event_date);
        eventDate.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    new DatePickerDialog(CreateEventActivity.this, new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                            eventDate.setText(year + "/" + (month + 1) + "/" + day);
                        }
                    }, 2011, 1, 1).show();
                }
            }
        });
    }

    private void setEventTimeListener() {
        eventTime = findViewById(R.id.event_time);
        eventTime.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    new TimePickerDialog(CreateEventActivity.this, new TimePickerDialog.OnTimeSetListener() {
                        @Override
                        public void onTimeSet(TimePicker timePicker, int hour, int minutes) {
                            eventTime.setText(hour + ":" + minutes + "PM");
                        }
                    }, 1, 1, false).show();
                }
            }
        });
    }

    private void setEventCancelListener() {
        eventCancel = findViewById(R.id.cancel_event);
        eventCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void setEventCreateListener() {
        eventCreate = findViewById(R.id.create_event);
        eventCreate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createEvent();
            }
        });
    }

    private void createEvent() {
        boolean isEmpty = false;
        if (!isInputFilled(eventLocation))
            isEmpty = true;
        if (!isInputFilled(eventTitle))
            isEmpty = true;
        if (!isInputFilled(eventDate))
            isEmpty = true;
        if (!isInputFilled(eventTime))
            isEmpty = true;
        if (!isInputFilled(eventAttendanceLimit))
            isEmpty = true;

        if (isEmpty) return;

        eventData.setUserID(AccessToken.getCurrentAccessToken().getUserId());
        eventData.setCreatorID(AccessToken.getCurrentAccessToken().getUserId());
        eventData.setTitle(eventTitle.getText().toString());
        eventData.setDate(eventDate.getText().toString());
        eventData.setTime(eventTime.getText().toString());
        eventData.setAttendanceLimit(Integer.parseInt(eventAttendanceLimit.getText().toString()));
        eventData.setIsActive(1);

        String currentDateTime = Constants.GetCurrentDateTime();
        eventData.setCreatedDate(currentDateTime);
        eventData.setModifiedDate(currentDateTime);

        TaskBundle taskBundle = new TaskBundle();
        taskBundle.acmBundle = clientManager;
        taskBundle.eventBundle = eventData;
        new CreateEventTask(this).execute(taskBundle);
    }

    private boolean isInputFilled(EditText editText) {
        if (editText.getText().toString().trim().isEmpty()) {
            editText.setError("Required field");
            return false;
        }
        else {
            editText.setError(null);
        }
        return true;
    }

    public static class CreateEventTask extends AsyncTask<TaskBundle, Void, Void> {
        WeakReference<CreateEventActivity> wrEvent;
        CreateEventTask(CreateEventActivity activity) {
            wrEvent = new WeakReference<>(activity);
        }

        @Override
        protected Void doInBackground(TaskBundle... taskBundles) {
            DynamoDBManager.saveEvent(taskBundles[0].eventBundle, taskBundles[0].acmBundle);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            CreateEventActivity eventActivity = wrEvent.get();
            eventActivity.finish();
        }
    }
}
