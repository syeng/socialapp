package com.syeng.socialapp.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.facebook.login.LoginManager;
import com.syeng.socialapp.FacebookLoginActivity;
import com.syeng.socialapp.R;

public class SettingsFragment extends Fragment {
    private View viewStay;

    public static SettingsFragment newInstance() {
        return new SettingsFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (viewStay == null)
            viewStay = inflater.inflate(R.layout.fragment_settings, container, false);

        Button logoutFb = viewStay.findViewById(R.id.logout_facebook_btn);
        logoutFb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LoginManager.getInstance().logOut();
                Intent intent = new Intent(getContext(), FacebookLoginActivity.class);
                startActivity(intent);
                getActivity().finish();
                /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    CookieManager.getInstance().removeAllCookies(new ValueCallback<Boolean>() {
                        @Override
                        public void onReceiveValue(Boolean aBoolean) {
                            if (aBoolean) {
                                LoginManager.getInstance().logOut();
                                Intent intent = new Intent(getContext(), FacebookLoginActivity.class);
                                startActivity(intent);
                                getActivity().finish();
                            }
                        }
                    });
                }
                else {
                    LoginManager.getInstance().logOut();
                    CookieSyncManager.getInstance().startSync();
                    CookieManager.getInstance().removeAllCookie();
                    CookieManager.getInstance().removeSessionCookie();
                    CookieSyncManager.getInstance().stopSync();
                    CookieSyncManager.getInstance().sync();
                    Intent intent = new Intent(getContext(), FacebookLoginActivity.class);
                    startActivity(intent);
                    getActivity().finish();
                }*/
            }
        });

        return viewStay;
    }
}
