package com.syeng.socialapp.fragments;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.syeng.socialapp.AmazonClientManager;
import com.syeng.socialapp.Constants;
import com.syeng.socialapp.DynamoDBManager;
import com.syeng.socialapp.R;

public class EventInformationFragment extends Fragment {
    private View viewStay;
    private Button cancelBtn;
    private Button actionBtn;

    class TaskBundle {
        AmazonClientManager amazonClientManager;
        DynamoDBManager.Events event;
    }

    public static EventInformationFragment newInstance() {
        return new EventInformationFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (viewStay == null)
            viewStay = inflater.inflate(R.layout.fragment_event_information, container, false);

        Bundle event = getArguments();

        ((TextView)viewStay.findViewById(R.id.name_text)).setText(event.getString("Name"));
        ((TextView)viewStay.findViewById(R.id.location_text)).setText(event.getString("Location"));
        ((TextView)viewStay.findViewById(R.id.date_text)).setText(event.getString("Date"));
        ((TextView)viewStay.findViewById(R.id.time_text)).setText(event.getString("Time"));
        ((TextView)viewStay.findViewById(R.id.al_text)).setText(String.valueOf(event.getInt("AL")));

        setCancelBtnListener();
        setActionBtnListener();

        return viewStay;
    }

    private void setCancelBtnListener() {
        cancelBtn = viewStay.findViewById(R.id.cancel_btn);
        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (getFragmentManager() != null) {
                    Fragment eventInfoFrag = getFragmentManager().findFragmentByTag(Constants.EVENT_INFO_TAG_FRAGMENT);
                    getFragmentManager().beginTransaction().remove(eventInfoFrag).commit();

                    Fragment googleMapsFrag = getFragmentManager().findFragmentByTag(Constants.GOOGLE_MAPS_TAG_FRAGMENT);
                    ((GoogleMapsFragment) googleMapsFrag).toggleOverlay(true);
                }
            }
        });
    }

    private void setActionBtnListener() {
        actionBtn = viewStay.findViewById(R.id.action_btn);
        actionBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
    }

    /*private static class SaveEventTask extends AsyncTask<GoogleMapsFragment.TaskBundle, Void, Void> {
        @Override
        protected Void doInBackground(GoogleMapsFragment.TaskBundle... taskBundles) {

        }
    }*/
}
