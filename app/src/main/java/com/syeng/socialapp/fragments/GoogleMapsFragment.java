package com.syeng.socialapp.fragments;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Point;
import android.location.Location;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.TileOverlay;
import com.google.android.gms.maps.model.TileOverlayOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.maps.android.heatmaps.HeatmapTileProvider;
import com.syeng.socialapp.AmazonClientManager;
import com.syeng.socialapp.Constants;
import com.syeng.socialapp.CreateEventActivity;
import com.syeng.socialapp.DynamoDBManager;
import com.syeng.socialapp.R;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

public class GoogleMapsFragment extends Fragment implements OnMapReadyCallback {

    private View instanceView;
    private AmazonClientManager clientManager = null;
    private GoogleMap maps;
    private Button heatmapBtn;
    private Button createEventBtn;
    private SparseArray<Marker> mapMarkers = new SparseArray<>();
    private TileOverlay heatMapOverlay;
    private GoogleMapsFragment fragmentContext;

    //Used to send into AsyncTask
    class TaskBundle {
        private AmazonClientManager acmBundle;
        private LatLng latLng;
    }

    public static GoogleMapsFragment newInstance() {
        return new GoogleMapsFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        clientManager = new AmazonClientManager(getContext());
    }

    @Override
    public void onResume() {
        super.onResume();
        //mapView.onResume();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        fragmentContext = this;

        if (instanceView == null)
            instanceView = inflater.inflate(R.layout.fragment_google_maps, container, false);

        setHeatmapBtnListener();
        setCreateEventBtnListener();

        return instanceView;
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        MapView mapView = view.findViewById(R.id.google_maps);
        if (mapView != null) {
            mapView.onCreate(null);
            mapView.onResume();
            mapView.getMapAsync(this);
        }
    }



    @SuppressLint("MissingPermission")
    @Override
    public void onMapReady(GoogleMap googleMap) {
        maps = googleMap;

        if (isAdded()) {
            LocationServices.getFusedLocationProviderClient(getContext())
                    .getLastLocation()
                    .addOnSuccessListener(getActivity(),
                            new OnSuccessListener<Location>() {
                @Override
                public void onSuccess(Location location) {
                    if (location != null) {
                        double longitude = location.getLongitude();
                        double latitude = location.getLatitude();
                        LatLng latlngTask = new LatLng(latitude, longitude);

                        maps.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                            @Override
                            public boolean onMarkerClick(Marker marker) {
                                Object objMarker = marker.getTag();
                                if (objMarker instanceof DynamoDBManager.Events) {
                                    if (getFragmentManager() != null) {

                                        Bundle event = new Bundle();
                                        event.putString("Name", ((DynamoDBManager.Events) objMarker).getTitle());
                                        event.putString("Location", ((DynamoDBManager.Events) objMarker).getAddress());
                                        event.putString("Date", ((DynamoDBManager.Events) objMarker).getDate());
                                        event.putString("Time", ((DynamoDBManager.Events) objMarker).getTime());
                                        event.putInt("AL", ((DynamoDBManager.Events) objMarker).getAttendanceLimit());
                                        EventInformationFragment eventInformationFragment = EventInformationFragment.newInstance();
                                        eventInformationFragment.setArguments(event);

                                        getFragmentManager().beginTransaction()
                                                .add(R.id.frame_container, eventInformationFragment, Constants.EVENT_INFO_TAG_FRAGMENT)
                                                .commit();
                                        Point mapPoint = maps.getProjection().toScreenLocation(new LatLng(marker.getPosition().latitude, marker.getPosition().longitude));
                                        mapPoint.set(mapPoint.x, mapPoint.y+ (instanceView.getHeight()/4));
                                        maps.animateCamera(CameraUpdateFactory.newLatLng(maps.getProjection().fromScreenLocation(mapPoint)), 500, null);

                                        toggleOverlay(false);
                                    }

                                }
                                return true;
                            }
                        });

                        //TODO: Add permissions
                        maps.setMyLocationEnabled(true);

                        maps.moveCamera(CameraUpdateFactory.newLatLngZoom(latlngTask, 12));

                        TaskBundle taskBundle = new TaskBundle();
                        taskBundle.acmBundle = clientManager;
                        taskBundle.latLng = latlngTask;

                        new GetLocationActiveUsersByRadius(fragmentContext).execute(taskBundle);
                    }
                }
            });
        }
    }

    private static class GetLocationActiveUsersByRadius
            extends AsyncTask<TaskBundle, Void, GetLocationActiveUsersByRadius.DdbBundle> {

        private WeakReference<GoogleMapsFragment> gMFragmentReference;

        class DdbBundle {
            private List<DynamoDBManager.Users> users;
            private List<DynamoDBManager.Events> events;
        }

        GetLocationActiveUsersByRadius (GoogleMapsFragment context) {
            gMFragmentReference = new WeakReference<>(context);
        }

        @Override
        protected DdbBundle doInBackground(TaskBundle... taskBundles) {
            DdbBundle ddbBundle = new DdbBundle();
            ddbBundle.users = DynamoDBManager.getLocationActiveUsersByRadius(
                    taskBundles[0].acmBundle,
                    taskBundles[0].latLng.longitude,
                    taskBundles[0].latLng.latitude);
            ddbBundle.events = DynamoDBManager.getActiveEvents(taskBundles[0].acmBundle);

            return ddbBundle;
        }

        @Override
        protected void onPostExecute(DdbBundle ddbBundle) {
            GoogleMapsFragment fragmentReference = gMFragmentReference.get();

            SparseArray<Marker> sparseArray = new SparseArray<>();
            List<LatLng> userPositionList = new ArrayList<>();

            for (int user = 0; user < ddbBundle.users.size(); user++) {
                LatLng userLatLng = new LatLng(ddbBundle.users.get(user).getLatitude(),
                                            ddbBundle.users.get(user).getLongitude());

                Marker marker = fragmentReference.maps
                        .addMarker(new MarkerOptions()
                        .position(userLatLng)
                        .anchor(0.5f, 0.5f)
                        .icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_launcher_round)));

                sparseArray.append(user, marker);
                userPositionList.add(userLatLng);
            }

            for (int event = 0; event < ddbBundle.events.size(); event++) {
                LatLng eventLatLng = new LatLng(ddbBundle.events.get(event).getLatitude(),
                        ddbBundle.events.get(event).getLongitude());

                Marker marker = fragmentReference.maps
                        .addMarker(new MarkerOptions()
                            .position(eventLatLng)
                            .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));
                marker.setTag(ddbBundle.events.get(event));
            }

            /*//Fake icons
            fragmentReference.maps.addMarker(new MarkerOptions()
                    .position(new LatLng(33.649570, -117.839663)));
            fragmentReference.maps.addMarker(new MarkerOptions()
                    .position(new LatLng(33.648650, -117.838107))
                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));*/

            fragmentReference.mapMarkers = sparseArray;

            HeatmapTileProvider provider = new HeatmapTileProvider.Builder()
                    .data(userPositionList)
                    .radius(50)
                    .build();
            fragmentReference.heatMapOverlay = fragmentReference.maps
                    .addTileOverlay(new TileOverlayOptions().tileProvider(provider));
            fragmentReference.heatMapOverlay.setVisible(false);
        }
    }

    private void setHeatmapBtnListener() {
        heatmapBtn = instanceView.findViewById(R.id.heatmap_btn);
        heatmapBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String heatmapBtnText = heatmapBtn.getText().toString();
                if (heatmapBtnText.equals("Heatmap Off")) {
                    heatmapBtn.setText("Heatmap On");

                    for (int i = 0; i < mapMarkers.size(); i++) {
                        mapMarkers.get(i).setVisible(true);
                    }

                    heatMapOverlay.setVisible(false);
                }
                else {
                    heatmapBtn.setText("Heatmap Off");

                    for (int i = 0; i < mapMarkers.size(); i++) {
                        mapMarkers.get(i).setVisible(false);
                    }

                    heatMapOverlay.setVisible(true);
                }
            }
        });
    }

    private void setCreateEventBtnListener() {
        createEventBtn = instanceView.findViewById(R.id.create_event_btn);
        createEventBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), CreateEventActivity.class);
                startActivityForResult(intent, Constants.CREATE_EVENT_REQUEST);
            }
        });
    }

    public void toggleOverlay(boolean isShowing) {
        if (!isShowing) {
            heatmapBtn.setVisibility(View.INVISIBLE);
            createEventBtn.setVisibility(View.INVISIBLE);
            maps.setMyLocationEnabled(false);
        }
        else {
            heatmapBtn.setVisibility(View.VISIBLE);
            createEventBtn.setVisibility(View.VISIBLE);
            maps.setMyLocationEnabled(true);
        }
    }

    private GoogleMap getMap() {
        return maps;
    }
}
